##!/bin/sh
# Si no se ejecuta de forma interactiva, no haga nada
case $- in
	*i*) ;;
	*) return;;
esac

set -o allexport
PAGER="less"                             # Paginador predeterminado
EDITOR="nvim"                             # Editor de texto predeterminado
TERM="st"                                   # Emulador de consola predeterminado
VISUAL="$EDITOR"                         # Igual valor que EDITOR
BROWSER="firefox-bin"                        # Navegador web predeterminado
READER="zathura"                         # Lector de documentos predeterminado
IMAGEVIEWER="imv"                       # Visor de imanenes predeterminado
FILE="nnn"                               # Gestor de archivos
PATH="/home/loftur/.local/bin:$PATH"

#Variables de entorno para NNN
NNN_OPENER="xdg-open"
NNN_FALLBACK_OPENER="xdg-open"                  # Apertura de archivos
NNN_OPTS="dU"                                 # Parámetros para ejecutar NNN
NNN_PLUG="n:nuke;p:preview-tabbed;m:nmount;i:imgview;a:mtpmount"       # Plugins
NNN_USE_EDITOR=1                                # Usar el $EDITOR para abrir archivos de texto
NNN_TRASH=1                                     # Mover archivos a papelera en lugar de eliminar definitivamente
NNN_READER="zathura"                            # Lector de documentos predeterminado
NNN_FIFO=/tmp/nnn.fifo                          # Pluggin para vista previa
NNN_ARCHIVE="\\.(7z|a|ace|alz|arc|arj|bz|bz2|cab|cpio|deb|gz|jar|lha|lz|lzh|lzma|lzo|rar|rpm|rz|t7z|tar|tbz|tbz2|tgz|tlz|txz|tZ|tzo|war|xpi|xz|Z|zip)$"

#autologin on tty1
if [ -z "$DISPLAY" ] && [ "$(tty)" = "/dev/tty1" ]; then
        exec startx
fi

# D-Bus session active
if which dbus-launch >/dev/null && test -z "$DBUS_SESSION_BUS_ADDRESS"; then
        eval `dbus-launch --sh-syntax --exit-with-session`
fi

#autologin on tty1
if [ -z "$DISPLAY" ] && [ "$(tty)" = "/dev/tty1" ]; then
        exec startx
fi

# D-Bus session active
if which dbus-launch >/dev/null && test -z "$DBUS_SESSION_BUS_ADDRESS"; then
        eval `dbus-launch --sh-syntax --exit-with-session`
fi

. "$HOME/.cargo/env"
