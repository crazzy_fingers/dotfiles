/* See LICENSE file for copyright and license details. */
#include <X11/XF86keysym.h>

/* appearance */
static const unsigned int borderpx  = 1;        /* border pixel of windows */
static const unsigned int snap      = 32;       /* snap pixel */
static const int showbar            = 1;        /* 0 means no bar */
static const int topbar             = 1;        /* 0 means bottom bar */
static const char *fonts[]          = { "Hack Nerd Font:Style=Regular:size=14:pixelsize=14" };
static const char dmenufont[]       = "Hack Nerd Font:Style=Regular:size=12";
static const char col_gray1[]       = "#222222";
static const char col_gray2[]       = "#444444";
static const char col_gray3[]       = "#bbbbbb";
static const char col_gray4[]       = "#eeeeee";
static const char col_cyan[]        = "#005577";
static const char col_black[]       = "#000000";
static const char col_red[]         = "#ff0000";
static const char col_yellow[]      = "#ffff00";
static const char col_white[]       = "#ffffff";
/* my colors*/
static const char col_1[]  	    = "#181c24";
static const char col_2[]  	    = "#000000";
static const char col_3[]	    = "#d7d7d7";
static const char col_4[]   	    = "#5E8485";

static const char *colors[][3]      = {
	/*               fg         bg         border   */
	[SchemeNorm] = { col_3,   col_1,      col_1 },
	[SchemeSel]  = { col_3,   col_2,      col_3 },
};

/* tagging */
static const char *tags[] = { "1:", "2:", "3:", "4:", "5:","6:","7:","8:", "9:" };
static const Rule rules[] = {
	/* xprop(1):
	 *	WM_CLASS(STRING) = instance, class
	 *	WM_NAME(STRING) = title
	 */
	/* class      instance    title       tags mask     isfloating   monitor */
	{ "Gimp",     NULL,       NULL,       0,            1,           -1 },
/*	{ "Firefox",  NULL,       NULL,       1 << 8,       0,           -1 },*/
	{ "firefox",  NULL,       NULL,       1 << 3,	    0,           -1 },
	{ "TelegramDesktop",  NULL,       NULL,       1 << 4,       0,           -1 },
};

/* layout(s) */
static const float mfact     = 0.55; /* factor of master area size [0.05..0.95] */
static const int nmaster     = 1;    /* number of clients in master area */
/*static const int resizehints = 1; */  /* 1 means respect size hints in tiled resizals */
static const int lockfullscreen = 1; /* 1 will force focus on the fullscreen window */
static const int resizehints = 0;    /* 1 means respect size hints in tiled resizals */

static const Layout layouts[] = {
	/* symbol     arrange function */
	{ "[]=",      tile },    /* first entry is default */
	{ "><>",      NULL },    /* no layout function means floating behavior */
	{ "[M]",      monocle },
};

/* key definitions */
#define MODKEY Mod4Mask
#define TAGKEYS(KEY,TAG) \
	{ MODKEY,                       KEY,      view,           {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask,           KEY,      toggleview,     {.ui = 1 << TAG} }, \
	{ MODKEY|ShiftMask,             KEY,      tag,            {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask|ShiftMask, KEY,      toggletag,      {.ui = 1 << TAG} },

/* helper for spawning shell commands in the pre dwm-5.0 fashion */
#define SHCMD(cmd) { .v = (const char*[]){ "/bin/sh", "-c", cmd, NULL } }

/* commands */
static char dmenumon[2] = "0"; /* component of dmenucmd, manipulated in spawn() */
static const char *dmenucmd[] = { "dmenu_run", "-m", dmenumon, "-fn", dmenufont, "-nb", col_gray1, "-nf", col_gray3, "-sb", col_cyan, "-sf", col_gray4, NULL };
static const char *roficmd[] = { "rofi", "-show", "run", NULL };
static const char *termcmd[]  = { "st", NULL };
static const char *mutevol[]          = { "amixer", "set", "Master", "toggle",  NULL };
static const char *downvol[]       = { "amixer", "-q","sset","Master","5%-", NULL };
static const char *upvol[]         = { "amixer", "-q","sset","Master","3%+", NULL };
static const char *dmenurecord[]   = { "/home/loftur/.local/bin/grabar.sh", NULL };
static const char *dmenuapagar[]   = { "/home/loftur/.local/bin/sesion.sh", NULL };
static const char *nnncmd[]        = { "st","-t", "nnn - Gestor de archivos", "-e", "nnn","-dU", NULL };
static const char *downbright[]       = { "xbacklight", "-dec","10", NULL };
static const char *upbright[]       = { "xbacklight", "-inc","10", NULL };


static Key keys[] = {
	/* modifier                     key        function        argument */
	{ MODKEY,                       XK_d,      spawn,          {.v = roficmd } },
	{ MODKEY,             		XK_Return, spawn,          {.v = termcmd } },
	{ MODKEY,                       XK_b,      togglebar,      {0} },
	{ MODKEY|ShiftMask,             XK_j,      rotatestack,    {.i = +1 } },
	{ MODKEY|ShiftMask,             XK_k,      rotatestack,    {.i = -1 } },
 	{ MODKEY,                       XK_j,      focusstack,     {.i = +1 } },
	{ MODKEY,                       XK_k,      focusstack,     {.i = -1 } },
	{ MODKEY,                       XK_i,      incnmaster,     {.i = +1 } },
	{ MODKEY|ShiftMask,             XK_i,      incnmaster,     {.i = -1 } },
	{ MODKEY,                       XK_h,      setmfact,       {.f = -0.05} },
	{ MODKEY,                       XK_l,      setmfact,       {.f = +0.05} },
	{ MODKEY|ShiftMask,             XK_Return, zoom,           {0} },
	{ MODKEY,                       XK_Tab,    view,           {0} },
	{ MODKEY|ShiftMask,             XK_q,      killclient,     {0} },
	{ MODKEY,                       XK_t,      setlayout,      {.v = &layouts[0]} },
	{ MODKEY,                       XK_f,      setlayout,      {.v = &layouts[1]} },
	{ MODKEY,                       XK_m,      setlayout,      {.v = &layouts[2]} },
	{ MODKEY|ShiftMask,             XK_f,      fullscreen,     {0} },
	{ MODKEY,                       XK_space,  setlayout,      {0} },
	{ MODKEY|ShiftMask,             XK_space,  togglefloating, {0} },
	{ MODKEY,                       XK_0,      view,           {.ui = ~0 } },
	{ MODKEY|ShiftMask,             XK_0,      tag,            {.ui = ~0 } },
	{ MODKEY,                       XK_comma,  focusmon,       {.i = -1 } },
	{ MODKEY,                       XK_period, focusmon,       {.i = +1 } },
	{ MODKEY|ShiftMask,             XK_comma,  tagmon,         {.i = -1 } },
	{ MODKEY|ShiftMask,             XK_period, tagmon,         {.i = +1 } },
	TAGKEYS(                        XK_1,                      0)
	TAGKEYS(                        XK_2,                      1)
	TAGKEYS(                        XK_3,                      2)
	TAGKEYS(                        XK_4,                      3)
	TAGKEYS(                        XK_5,                      4)
	TAGKEYS(                        XK_6,                      5)
	TAGKEYS(                        XK_7,                      6)
	TAGKEYS(                        XK_8,                      7)
	TAGKEYS(                        XK_9,                      8)
	{ MODKEY|ShiftMask,             XK_e,      quit,           {0} },

 /*      Comandos personalizados   */
 	{ MODKEY,                       XK_n,                      spawn,          {.v = nnncmd } },
 	{ MODKEY,                       XK_r,                      spawn,          {.v = dmenurecord } },
 	{ MODKEY,                       XK_s,                      spawn,          {.v = dmenuapagar } },
 	
/*	Atajos Multimedia	*/
	{ 0,                       	XF86XK_AudioLowerVolume, spawn, {.v = downvol } },
	{ 0,                       	XF86XK_AudioRaiseVolume, spawn, {.v = upvol   } },
	{ 0,                       	XF86XK_AudioMute, spawn, {.v = mutevol } },
	{ 0,                       	XF86XK_MonBrightnessUp, spawn, {.v = upbright   } },
	{ 0,                       	XF86XK_MonBrightnessDown, spawn, {.v = downbright   } },

/*      Modos de captura de pantalla    */
{ 0,                  XK_Print,  spawn, SHCMD("scrot /tmp/'%F_%T.png' -e 'xclip -selection c -t image/png < $f'; sleep 1; exec herbe 'Pantalla en portapapeles'")     }, // Captura de pantalla al portapapeles
{ ControlMask,        XK_Print,  spawn, SHCMD("scrot -u /tmp/'%F_%T.png' -e 'xclip -selection c -t image/png < $f'; sleep 1; exec herbe 'Ventana en portapapeles'") }, // Ventana seleccionada al portapapeles
{ ShiftMask,          XK_Print,  spawn, SHCMD("sleep 1; scrot  -s /tmp/'%F_%T.png' -e 'xclip -selection c -t image/png < $f'; sleep 1; exec herbe 'Área en portapapeles'") }, // Area seleccionada al portapapeles

{ MODKEY,             XK_Print,  spawn, SHCMD("scrot  'c_%F_%H%M%S_$wx$h.png' -e     'mv $f /home/loftur/Pictures/screenshots/'; sleep 1; exec herbe 'Pantalla en ~/Capturas'") },//Captura de pantalla
{ MODKEY|ControlMask, XK_Print,  spawn, SHCMD("scrot  -u 'v_%F_%H%M%S_$wx$h.png'     -e 'mv $f /home/loftur/Pictures/screenshots/'; sleep 1; exec herbe 'Ventana en ~/Capturas'") }    ,//Captura de ventana
{ MODKEY|ShiftMask,   XK_Print,  spawn, SHCMD("sleep 1; scrot  -s 'a_%F_%H%M%S_$wx$h.png' -e 'mv $f /home/loftur/Pictures/screenshots/'; exec herbe 'Área en ~/Capturas'") },//    Captura de Area



};

/* button definitions */
/* click can be ClkTagBar, ClkLtSymbol, ClkStatusText, ClkWinTitle, ClkClientWin, or ClkRootWin */
static Button buttons[] = {
	/* click                event mask      button          function        argument */
	{ ClkTagBar,            MODKEY,         Button1,        tag,            {0} },
	{ ClkTagBar,            MODKEY,         Button3,        toggletag,      {0} },
	{ ClkWinTitle,          0,              Button2,        zoom,           {0} },
	{ ClkStatusText,        0,              Button2,        spawn,          {.v = termcmd } },
	{ ClkClientWin,         MODKEY,         Button1,        movemouse,      {0} },
	{ ClkClientWin,         MODKEY,         Button2,        togglefloating, {0} },
	{ ClkClientWin,         MODKEY,         Button3,        resizemouse,    {0} },
	{ ClkTagBar,            0,              Button1,        view,           {0} },
	{ ClkTagBar,            0,              Button3,        toggleview,     {0} },
	{ ClkTagBar,            MODKEY,         Button1,        tag,            {0} },
	{ ClkTagBar,            MODKEY,         Button3,        toggletag,      {0} },
};

